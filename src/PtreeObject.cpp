#include "PtreeObject.hpp"
#include <sstream>
#include <boost/property_tree/json_parser.hpp>

using namespace std;
using namespace boost::property_tree;

namespace zppdata {

PtreeObject::PtreeObject()
{
}

void PtreeObject::loadFromPtree(boost::property_tree::ptree const & pTree)
{
	for(auto key : keys) {
		auto itr = pTree.find(key);
		if(itr != pTree.not_found()) {
			fromFuncs[key](this, itr->second);
		}
	}
}

boost::property_tree::ptree PtreeObject::dumpToPtree()
{
	ptree pTree;
	for(auto & key : keys) {
		ptree childTree = toFuncs[key](this);
		pTree.push_back(make_pair(key, childTree));
	}
	return pTree;
}

std::string PtreeObject::dumpToJson()
{
    ptree dumpedPtree = dumpToPtree();
	stringstream jsonStrStream;
	write_json(jsonStrStream, dumpedPtree);
	return jsonStrStream.str();
}

void fromPtree(boost::property_tree::ptree const & pTree, PtreeObject & var)
{
	var.loadFromPtree(pTree);
}

boost::property_tree::ptree toPtree(PtreeObject & var)
{
	return var.dumpToPtree();
}

} // namespace zppdata
