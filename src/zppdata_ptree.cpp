#include "zppdata_ptree.hpp"

using namespace std;
using namespace boost::property_tree;

namespace zppdata {

double const DOUBLE_ERROR = numeric_limits<double>::signaling_NaN();
float const FLOAT_ERROR = numeric_limits<float>::signaling_NaN();
int64_t const INT64_T_ERROR = numeric_limits<int64_t>::max();
int32_t const INT32_T_ERROR = numeric_limits<int32_t>::max();

void fromPtree(boost::property_tree::ptree const & pTree, double & valueOut)
{
	try {
		valueOut = pTree.get_value<double>();
	}
	catch(std::exception) {
		valueOut = DOUBLE_ERROR;
	}
}

boost::property_tree::ptree toPtree(double value)
{
	boost::property_tree::ptree pTree;
	pTree.put("", value);
	return pTree;
}

void fromPtree(boost::property_tree::ptree const & pTree, float & valueOut)
{
	try {
		valueOut = pTree.get_value<float>();
	}
	catch(std::exception) {
		valueOut = FLOAT_ERROR;
	}
}

boost::property_tree::ptree toPtree(float value)
{
	boost::property_tree::ptree pTree;
	pTree.put("", value);
	return pTree;
}

void fromPtree(boost::property_tree::ptree const & pTree, int64_t & valueOut)
{
	try {
		valueOut = pTree.get_value<int64_t>();
	}
	catch(std::exception) {
		valueOut = INT64_T_ERROR;
	}
}

boost::property_tree::ptree toPtree(int64_t value)
{
	boost::property_tree::ptree pTree;
	pTree.put("", value);
	return pTree;
}

void fromPtree(boost::property_tree::ptree const & pTree, int32_t & valueOut)
{
	try {
		valueOut = pTree.get_value<int32_t>();
	}
	catch(std::exception) {
		valueOut = INT32_T_ERROR;
	}
}

boost::property_tree::ptree toPtree(int32_t value)
{
	boost::property_tree::ptree pTree;
	pTree.put("", value);
	return pTree;
}

void fromPtree(boost::property_tree::ptree const & pTree, bool & valueOut)
{
	valueOut = pTree.get_value<bool>();
}

boost::property_tree::ptree toPtree(bool value)
{
	boost::property_tree::ptree pTree;
	pTree.put("", value);
	return pTree;
}

void fromPtree(boost::property_tree::ptree const & pTree, std::string & valueOut)
{
	valueOut = pTree.get_value<std::string>();
}

boost::property_tree::ptree toPtree(std::string const & value)
{
	boost::property_tree::ptree pTree;
	pTree.put("", value);
	return pTree;
}

} // namespace zppdata
