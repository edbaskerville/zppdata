#ifndef __zppdata_util__
#define __zppdata_util__

#include <vector>
#include <string>
#include <sstream>

namespace zppdata {

std::string strprintf(char const * format, ...);
std::string strjoin(std::vector<std::string> const & strings, std::string sep);
std::string escapeJsonString(std::string const & input);

} // namespace zppdata

#endif // #define __zppdata_util__
