#ifndef __zppdata__Database__
#define __zppdata__Database__

#include <string>
#include <vector>
#include <unordered_map>
#include <sqlite3.h>
#include <unistd.h>

namespace zppdata {

class Database;
class DBTable;
class DBRow;

class Database
{
friend class DBTable;
friend class DBQuery;
public:
	Database();
	Database(std::string const & filename);
	~Database();
	
	void beginTransaction() const;
	bool commit() const;
	bool commitWithRetry(useconds_t useconds, useconds_t timeout, std::ostream & errStream);
	void execute(std::string const & sqlCmd) const;
private:
	sqlite3 * db;
};

enum class DBType
{
	INTEGER,
	REAL,
	TEXT
};

struct DBColumn
{
	DBColumn(std::string name, DBType type) : name(name), type(type) { }
	std::string name;
	DBType type;
};

class DBRow
{
public:
	void setNull(std::string const & key);
	
	void setInteger(std::string const & key, int64_t const & value);
	bool hasInteger(std::string const & key) const;
	int64_t getInteger(std::string const & key) const;
	
	void setReal(std::string const & key, double const & value);
	bool hasReal(std::string const & key) const;
	double getReal(std::string const & key) const;
	
	void setText(std::string const & key, std::string const & value);
	bool hasText(std::string const & key) const;
	std::string getText(std::string const & key) const;
private:
	std::unordered_map<std::string, int64_t> intMap;
	std::unordered_map<std::string, double> dblMap;
	std::unordered_map<std::string, std::string> strMap;
};

class DBTable
{
public:
	DBTable(Database * dbPtr, std::string const & name, std::vector<DBColumn> const & columns);
	~DBTable();
	std::string getName() const;
	bool hasColumn(std::string colName) const;
	int64_t columnCount() const;
	DBType getColumnType(std::string colName) const;
	DBColumn getColumn(std::string colName) const;
	DBColumn getColumn(int64_t index) const;
	
	void create() const;
	void insert(DBRow const & row);
	void insert(std::vector<std::string> const & row);
	void insert(std::vector<double> const & row);

private:
	Database * dbPtr;
	sqlite3_stmt * _ppStmt;
	std::string name;
	std::vector<DBColumn> columns;
	std::unordered_map<std::string, int64_t> indexMap;
	
	sqlite3_stmt * getPreparedStatement();
};

class DBQuery
{
public:
	DBQuery(Database * dbPtr, std::string const & query);
	~DBQuery();
	
	int bindNull(int column);
	int bindInteger(int column, int64_t value);
	int bindReal(int column, double value);
	int bindText(int column, std::string const & value);
	
	int step();
	int reset();
	
	bool isNull(int column);
	int64_t getInteger(int column);
	double getReal(int column);
	std::string getText(int column);
private:
	sqlite3_stmt * ppStmt;
};

} // namespace zppdata

#endif /* defined(__zppdata__Database__) */
