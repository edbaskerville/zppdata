#ifndef __zppdata_ptree__
#define __zppdata_ptree__

#include <vector>
#include <map>
#include <boost/property_tree/ptree.hpp>

namespace zppdata {

extern double const DOUBLE_ERROR;
extern float const FLOAT_ERROR;
extern int64_t const INT64_T_ERROR;
extern int32_t const INT32_T_ERROR;

void fromPtree(boost::property_tree::ptree const & pTree, double & valueOut);

boost::property_tree::ptree toPtree(double value);

void fromPtree(boost::property_tree::ptree const & pTree, float & valueOut);

boost::property_tree::ptree toPtree(float value);

void fromPtree(boost::property_tree::ptree const & pTree, int64_t & valueOut);

boost::property_tree::ptree toPtree(int64_t value);

void fromPtree(boost::property_tree::ptree const & pTree, int32_t & valueOut);

boost::property_tree::ptree toPtree(int32_t value);

void fromPtree(boost::property_tree::ptree const & pTree, int32_t & valueOut);

boost::property_tree::ptree toPtree(int32_t value);

void fromPtree(boost::property_tree::ptree const & pTree, bool & valueOut);

boost::property_tree::ptree toPtree(bool value);

void fromPtree(boost::property_tree::ptree const & pTree, std::string & valueOut);

boost::property_tree::ptree toPtree(std::string const & value);

template<typename T>
void fromPtree(boost::property_tree::ptree const & pTree, std::vector<T> & valueOut)
{
	valueOut.clear();
	
	// Load vector from value entries in ptree
	for(auto itr = pTree.begin(); itr != pTree.end(); itr++) {
		T item;
		fromPtree(itr->second, item);
		valueOut.push_back(item);
	}
}

template<typename T>
boost::property_tree::ptree toPtree(std::vector<T> const & vec)
{
	boost::property_tree::ptree pTree;
	for(auto item : vec) {
		pTree.push_back(std::make_pair("", toPtree(item)));
	}
	return pTree;
}

template<typename T>
void fromPtree(boost::property_tree::ptree const & pTree, std::map<std::string, T> & valueOut)
{
	valueOut.clear();
	
	// Load map from key-value entries in ptree
	for(auto itr = pTree.begin(); itr != pTree.end(); itr++) {
		T item;
		fromPtree(itr->second, item);
		valueOut[itr->first] = item;
	}
}

template<typename T>
boost::property_tree::ptree toPtree(std::map<std::string, T> & m)
{
	boost::property_tree::ptree pTree;
	for(auto item : m) {
		pTree.push_back(std::make_pair(item.first, toPtree(item.second)));
	}
	return pTree;
}

} // namespace zppdata

#endif /* defined(__zppdata_ptree__) */
