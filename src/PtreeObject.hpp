#ifndef __zppdata__PtreeObject__
#define __zppdata__PtreeObject__

#include <vector>
#include <map>
#include <boost/property_tree/ptree.hpp>
#include "zppdata_ptree.hpp"

namespace zppdata {

class PtreeObject;

typedef std::function<void(PtreeObject * owner, boost::property_tree::ptree const &)> FromFunction;
typedef std::function<boost::property_tree::ptree(PtreeObject * owner)> ToFunction;

void fromPtree(boost::property_tree::ptree const & pTree, PtreeObject & var);
boost::property_tree::ptree toPtree(PtreeObject & var);

class PtreeObject
{
public:
	PtreeObject();
	void loadFromPtree(boost::property_tree::ptree const & pTree);
	boost::property_tree::ptree dumpToPtree();
	std::string dumpToJson();

protected:
	std::vector<std::string> keys;
	std::map<std::string, FromFunction> fromFuncs;
	std::map<std::string, ToFunction> toFuncs;
	
	template<typename T>
	void define(std::string const & key, T & var)
	{
		keys.push_back(key);
		
		intptr_t offset = (intptr_t)(&var) - (intptr_t)this;
		
		fromFuncs[key] = [offset](PtreeObject * owner, boost::property_tree::ptree const & pTree) {
			T * varPtr = (T *)((intptr_t)owner + offset);
			fromPtree(pTree, *varPtr);
		};
		
		toFuncs[key] = [offset](PtreeObject * owner) {
			T * varPtr = (T *)((intptr_t)owner + offset);
			return toPtree(*varPtr);
		};
	}
};


} // namespace zppdata

#endif /* defined(__zppdata__Parameters__) */
