#include "Database.hpp"

#include <vector>
#include <iostream>
#include "zppdata_util.hpp"

using namespace std;

namespace zppdata {

DBTable::DBTable(Database * dbPtr, std::string const & name, std::vector<DBColumn> const & columns):
	dbPtr(dbPtr), _ppStmt(nullptr), name(name), columns(columns)
{
	if(int64_t(columns.size()) < 0) {
		throw std::runtime_error("Too many columns: 63-bit integer overflow in column count.");
	}
	
	for(size_t i = 0; i < columns.size(); i++) {
		indexMap[columns[i].name] = i;
	}
}

DBTable::~DBTable()
{
	if(_ppStmt != nullptr) {
		sqlite3_finalize(_ppStmt);
	}
}

sqlite3_stmt * DBTable::getPreparedStatement()
{
	if(_ppStmt == nullptr) {
		sqlite3 * sqliteDb = dbPtr->db;
		
		// Construct SQL insert statement with ? parameters
		string qMark("?");
		vector<string> qMarks;
		for(size_t i = 0; i < columns.size(); i++) {
			qMarks.push_back(qMark);
		}
		string sqlCmdStr = strprintf("INSERT INTO %s VALUES (%s)", name.c_str(), strjoin(qMarks, ", ").c_str());
		char const * sqlCmd = sqlCmdStr.c_str();
		
		// Create prepared statement
		int result = sqlite3_prepare_v2(sqliteDb, sqlCmd, -1, &_ppStmt, NULL);
		if(result) {
			throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
		}
	}
	return _ppStmt;
}

std::string DBTable::getName() const {
	return name;
}

bool DBTable::hasColumn(std::string colName) const {
	return indexMap.find(colName) != indexMap.end();
}

int64_t DBTable::columnCount() const {
	return columns.size();
}

DBType DBTable::getColumnType(std::string colName) const {
	return getColumn(colName).type;
}

DBColumn DBTable::getColumn(std::string colName) const {
	return columns[indexMap.at(colName)];
}

DBColumn DBTable::getColumn(int64_t index) const {
	return columns[index];
}

void DBRow::setNull(std::string const & key) {
	intMap.erase(key);
	dblMap.erase(key);
	strMap.erase(key);
}

void DBRow::setInteger(std::string const & key, int64_t const & value) {
	intMap[key] = value;
}

bool DBRow::hasInteger(std::string const & key) const {
	return intMap.find(key) != intMap.end();
}

int64_t DBRow::getInteger(std::string const & key) const {
	return intMap.at(key);
}

void DBRow::setReal(std::string const & key, double const & value) {
	dblMap[key] = value;
}

bool DBRow::hasReal(std::string const & key) const {
	return dblMap.find(key) != dblMap.end();
}

double DBRow::getReal(std::string const & key) const {
	return dblMap.at(key);
}

void DBRow::setText(std::string const & key, std::string const & value) {
	strMap[key] = value;
}

bool DBRow::hasText(std::string const & key) const {
	return strMap.find(key) != strMap.end();
}

std::string DBRow::getText(std::string const & key) const {
	return strMap.at(key);
}

Database::Database()
{
	db = NULL;
}

Database::Database(string const & filename) {
	if(filename == "") {
		db = NULL;
	}
	else {
		int result = sqlite3_open(filename.c_str(), &db);
		if(result) {
			throw std::runtime_error(string(sqlite3_errmsg(db)));
		}
	}
}

Database::~Database() {
	if(db != NULL) {
		sqlite3_close(db);
	}
}

void Database::beginTransaction() const {
	if(db == NULL) {
		return;
	}
	
	execute("BEGIN TRANSACTION");
}

bool Database::commit() const {
	if(db == NULL) {
		return true;
	}
	
	int result = sqlite3_exec(db, "COMMIT", NULL, NULL, NULL);
	if(result == SQLITE_BUSY) {
		return false;
	}
	else if(result) {
		throw std::runtime_error(string(sqlite3_errmsg(db)));
	}
	return true;
}

bool Database::commitWithRetry(useconds_t useconds, useconds_t timeout, std::ostream & errStream)
{
	useconds_t total = 0;
	bool done = false;
	while(!done) {
		done = commit();
		if(!done) {
			if(total > timeout) {
				break;
			}
			errStream << "Commit unsuccessful due to busy database; retrying in " << useconds << "us" << '\n';
			usleep(useconds);
		}
	}
	return done;
}

void Database::execute(std::string const & sqlCmd) const
{
	if(db == NULL) {
		return;
	}
	
	int result = sqlite3_exec(db, sqlCmd.c_str(), NULL, NULL, NULL);
	if(result) {
		throw std::runtime_error(string(sqlite3_errmsg(db)));
	}
}

void DBTable::create() const {
	sqlite3 * sqliteDb = dbPtr->db;
	
	if(sqliteDb == NULL) {
		return;
	}
	
	vector<string> columnStrs;
	for(size_t i = 0; i < columns.size(); i++) {
		string colName = columns[i].name;
		switch(columns[i].type) {
			case DBType::INTEGER:
				columnStrs.push_back(colName + " INTEGER");
				break;
			case DBType::REAL:
				columnStrs.push_back(colName + " REAL");
				break;
			case DBType::TEXT:
				columnStrs.push_back(colName + " TEXT");
				break;
		}
	}
	string sqlCmd = strprintf(
		"CREATE TABLE %s (%s)",
		getName().c_str(),
		strjoin(columnStrs, ", ").c_str()
	);
	
	dbPtr->execute(sqlCmd);
}

void DBTable::insert(DBRow const & row) {
	sqlite3 * sqliteDb = dbPtr->db;
	
	if(sqliteDb == NULL) {
		return;
	}
	
	// Bind values to prepared statement parameters
	sqlite3_stmt * ppStmt = getPreparedStatement();
	for(size_t i = 0; i < columns.size(); i++) {
		int result;
		string colName = columns[i].name;
		switch(columns[i].type) {
			case DBType::INTEGER:
				if(row.hasInteger(colName)) {
					result = sqlite3_bind_int64(ppStmt, int(i+1), row.getInteger(colName));
				}
				else {
					result = sqlite3_bind_null(ppStmt, int(i+1));
				}
				break;
			case DBType::REAL:
				if(row.hasReal(colName)) {
					result = sqlite3_bind_double(ppStmt, int(i+1), row.getReal(colName));
				}
				else {
					result = sqlite3_bind_null(ppStmt, int(i+1));
				}
				break;
			case DBType::TEXT:
				if(row.hasText(colName)) {
					string textStr = row.getText(colName);
					char const * text = textStr.c_str();
					result = sqlite3_bind_text(ppStmt, int(i+1), text, -1, SQLITE_TRANSIENT);
				}
				else {
					result = sqlite3_bind_null(ppStmt, int(i+1));
				}
				break;
		}
		if(result) {
			throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
		}
	}
	
	// Execute prepared statement
	int result = sqlite3_step(ppStmt);
	if(result != SQLITE_DONE) {
		throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
	}
	
	// Reset prepared statement for next step
	result = sqlite3_reset(ppStmt);
	if(result != SQLITE_OK) {
		throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
	}
}

void DBTable::insert(std::vector<string> const & row)
{
	if(columns.size() != row.size()) {
		throw std::runtime_error(
			strprintf("Row does not match column count in table %s.", name.c_str())
		);
	}
	
	sqlite3 * sqliteDb = dbPtr->db;
	
	if(sqliteDb == NULL) {
		return;
	}
	
	sqlite3_stmt * ppStmt = getPreparedStatement();
	
	// Bind values to prepared statement parameters
	for(size_t i = 0; i < columns.size(); i++) {
		int result = sqlite3_bind_text(ppStmt, int(i+1), row[i].c_str(), -1, SQLITE_TRANSIENT);
		if(result) {
			throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
		}
	}
	
	// Execute prepared statement
	int result = sqlite3_step(ppStmt);
	if(result != SQLITE_DONE) {
		throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
	}
}

void DBTable::insert(std::vector<double> const & row)
{
	if(columns.size() != row.size()) {
		throw std::runtime_error(
			strprintf("Row does not match column count in table %s.", name.c_str())
		);
	}
	
	sqlite3 * sqliteDb = dbPtr->db;
	
	if(sqliteDb == NULL) {
		return;
	}
	
	sqlite3_stmt * ppStmt = getPreparedStatement();
	
	// Bind values to prepared statement parameters
	for(size_t i = 0; i < columns.size(); i++) {
		int result = sqlite3_bind_double(ppStmt, int(i+1), row[i]);
		if(result) {
			throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
		}
	}
	
	// Execute prepared statement
	int result = sqlite3_step(ppStmt);
	if(result != SQLITE_DONE) {
		throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
	}
}

DBQuery::DBQuery(Database * dbPtr, std::string const & query)
{
	int result = sqlite3_prepare_v2(dbPtr->db, query.c_str(), -1, &ppStmt, NULL);
	if(result) {
		throw std::runtime_error(string(sqlite3_errmsg(dbPtr->db)));
	}
}

DBQuery::~DBQuery()
{
	sqlite3_finalize(ppStmt);
}

int DBQuery::bindNull(int column)
{
	return sqlite3_bind_null(ppStmt, int(column+1));
}

int DBQuery::bindInteger(int column, int64_t value)
{
	return sqlite3_bind_null(ppStmt, int(column+1));
}

int DBQuery::bindReal(int column, double value)
{
	return sqlite3_bind_double(ppStmt, int(column+1), value);
}

int DBQuery::bindText(int column, std::string const & value)
{
	return sqlite3_bind_text(ppStmt, int(column+1), value.c_str(), -1, SQLITE_TRANSIENT);
}

int DBQuery::step()
{
	return sqlite3_step(ppStmt);
}

int DBQuery::reset()
{
	return sqlite3_reset(ppStmt);
}

bool DBQuery::isNull(int column)
{
	return sqlite3_column_type(ppStmt, column) == SQLITE_NULL;
}

int64_t DBQuery::getInteger(int column)
{
	return sqlite3_column_int64(ppStmt, column);
}

double DBQuery::getReal(int column)
{
	return sqlite3_column_double(ppStmt, column);
}

std::string DBQuery::getText(int column)
{
	return string(reinterpret_cast<char const *>(
		sqlite3_column_text(ppStmt, column)
	));
}

} // namespace zppdata
