#include "catch.hpp"
#include "PtreeObject.hpp"

#include <boost/property_tree/json_parser.hpp>
#include <sstream>
#include <iostream>

using namespace boost::property_tree;
using namespace boost::property_tree::json_parser;
using namespace zppdata;
using namespace std;


static std::string srcJson = R"({
	"a" : 0.5,
	"b" : "hello there mr",
	"c" : 100,
	"d" : {
		"obj1" : {
			"a" : 0.1,
			"b" : null,
			"c" : {
				"x" : 0.1,
				"y" : 0.2,
				"z" : 0.3
			}
		},
		"obj2" : {
			"a" : 0.4,
			"b" : 0.5,
			"c" : {
				"x" : 0.4,
				"y" : 0.5,
				"z" : 0.6
			}
		
		}
	}
})";

class MySubObject : public PtreeObject
{
public:
	double a;
	double b;
	map<string, double> c;
	
	MySubObject()
	{
		define("a", a);
		define("b", b);
		define("c", c);
	}
};

class MyObject : public PtreeObject
{
public:
	double a;
	string b;
	int32_t c;
	map<string, MySubObject> d;
	
	MyObject()
	{
		define("a", a);
		define("b", b);
		define("c", c);
		define("d", d);
	}
};

TEST_CASE("PtreeObject works", "[PtreeObject]")
{
	stringstream ss(srcJson);
	ptree objPtree;
	read_json(ss, objPtree);
	
	MyObject myObj;
	myObj.loadFromPtree(objPtree);
	
	REQUIRE(myObj.a == 0.5);
	REQUIRE(myObj.b == "hello there mr");
	REQUIRE(myObj.c == 100);
	
	map<string, MySubObject> d = myObj.d;
	MySubObject obj1 = d["obj1"];
	REQUIRE(obj1.a == 0.1);
	REQUIRE(isnan(obj1.b));
	map<string, double> obj1c({{"x", 0.1}, {"y", 0.2}, {"z", 0.3}});
	REQUIRE(obj1.c == obj1c);
	
	MySubObject obj2 = d["obj2"];
	REQUIRE(obj2.a == 0.4);
	REQUIRE(obj2.b == 0.5);
	map<string, double> obj2c({{"x", 0.4}, {"y", 0.5}, {"z", 0.6}});
	REQUIRE(obj2.c == obj2c);
}
