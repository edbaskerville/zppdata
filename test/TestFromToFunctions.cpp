#include "catch.hpp"
#include "zppdata_ptree.hpp"
#include <boost/property_tree/ptree.hpp>

using namespace boost::property_tree;
using namespace zppdata;
using namespace std;

#define TEST_TO_PTREE(T, value) SECTION("toPtree(" #T ")") { T var = value; \
ptree pTree = toPtree(var); \
REQUIRE(pTree.get_value<T>() == value); }

#define TEST_FROM_PTREE(T, value) SECTION("fromPtree(" #T ")") { ptree pTree; \
pTree.put("", value); \
T var; \
fromPtree(pTree, var); \
REQUIRE(var == value); }


TEST_CASE("Simple data conversions")
{
	TEST_FROM_PTREE(bool, true);
	TEST_TO_PTREE(bool, true);
	
	TEST_FROM_PTREE(int64_t, -(int64_t(2) << 35));
	TEST_TO_PTREE(int64_t, -(int64_t(2) << 35));
	
	TEST_FROM_PTREE(int32_t, -(2 << 18));
	TEST_TO_PTREE(int32_t, -(2 << 18));
	
	TEST_FROM_PTREE(string, "hello");
	TEST_TO_PTREE(string, "hello");
}

TEST_CASE("Single-level containers")
{
	SECTION("fromPtree(vector<double>)")
	{
		vector<double> vals = {0.5, 0.6, 1.3, 1.4};
		
		ptree pTree;
		for(double val : vals) {
			ptree child;
			child.put("", val);
			pTree.push_back(std::make_pair("", child));
		}
		
		vector<double> var;
		fromPtree(pTree, var);
		REQUIRE(var == vals);
	}
	
	SECTION("toPtree(vector<double>)")
	{
		vector<double> srcVec = {0.5, 0.6, 1.3, 1.4};
		
		ptree pTree = toPtree(srcVec);
		vector<double> dstVec;
		fromPtree(pTree, dstVec);
		
		REQUIRE(srcVec == dstVec);
	}
	
	SECTION("fromPtree(map<string, double>)")
	{
		map<string, double> vals = {
			{"a", 0.5},
			{"b", 0.6},
			{"c", 1.3},
			{"d", 1.4}
		};
		
		ptree pTree;
		for(auto kv : vals) {
			pTree.put(kv.first, kv.second);
		}
		
		map<string, double> var;
		fromPtree(pTree, var);
		REQUIRE(var == vals);
	}
	
	SECTION("toPtree(map<string, double>)")
	{
		map<string, double> srcMap = {
			{"a", 0.5},
			{"b", 0.6},
			{"c", 1.3},
			{"d", 1.4}
		};
		
		ptree pTree = toPtree(srcMap);
		map<string, double> dstMap;
		fromPtree(pTree, dstMap);
		
		REQUIRE(srcMap == dstMap);
	}
}

TEST_CASE("Multi-level containers")
{
	SECTION("fromPtree(vector<vector<double>>)")
	{
		vector<vector<double>> vecs = {{0.5, 0.6}, {1.3, 1.4}};
		
		ptree pTree;
		for(auto vec : vecs) {
			ptree child;
			for(double val : vec) {
				ptree child2;
				child2.put("", val);
				child.push_back(std::make_pair("", child2));
			}
			pTree.push_back(std::make_pair("", child));
		}
		
		vector<vector<double>> var;
		fromPtree(pTree, var);
		REQUIRE(var == vecs);
	}
	
	SECTION("fromPtree(map<string, vector<double>>)")
	{
		map<string, vector<double>> srcMap = {{"a", {0.5, 0.6}}, {"b", {1.3, 1.4}}};
		
		ptree pTree;
		for(auto kv : srcMap) {
			ptree child;
			for(double val : kv.second) {
				ptree child2;
				child2.put("", val);
				child.push_back(std::make_pair("", child2));
			}
			pTree.push_back(std::make_pair(kv.first, child));
		}
		
		map<string, vector<double>> dstMap;
		fromPtree(pTree, dstMap);
		REQUIRE(srcMap == dstMap);
	}
	
	SECTION("fromPtree(map<string, map<string, double>>)")
	{
		map<string, map<string, double>> srcMap = {
			{"A", {{"a", 0.5}, {"b", 0.6}}},
			{"B", {{"c", 1.3}, {"d", 1.4}}}
		};
		
		ptree pTree;
		for(auto kv : srcMap) {
			ptree child;
			for(auto kv2 : kv.second) {
				child.put(kv2.first, kv2.second);
			}
			pTree.push_back(std::make_pair(kv.first, child));
		}
		
		map<string, map<string, double>> dstMap;
		fromPtree(pTree, dstMap);
		REQUIRE(srcMap == dstMap);
	}
}

